package cern.wintercampus.ex5

import cern.wintercampus.ex4.dataframe.DataFrameExamples
import cern.wintercampus.utils._
import org.apache.spark.sql.{DataFrame, SparkSession}

object ParquetExamples {
  def main(args: Array[String]): Unit = {
    val spark = createLocalSparkSession("ParquetExamples")

    val examples = DataFrameExamples(spark)
    val aggregatedDataset = examples.joinAndAggregateSqlForParquet(MoviesDatasetPath, MovieGenresDatasetPath)
    deleteOutputsIfExists()
    writeDf(aggregatedDataset)

    println("Going to read it back and do more aggregations!")
    val newReadDf = readDf(spark)
    newReadDf.printSchema()
    newReadDf.where("genre = 'Comedy'")
      .where("id < 20")
      .show()

    sleepAfterRun()
  }

  private def writeDf(aggregatedDataset: DataFrame): Unit = {
    aggregatedDataset.write.partitionBy("genre")
      .parquet("outputs/aggregated_df")
  }

  private def readDf(spark: SparkSession): DataFrame = {
    spark.read.parquet("outputs/aggregated_df")
  }
}

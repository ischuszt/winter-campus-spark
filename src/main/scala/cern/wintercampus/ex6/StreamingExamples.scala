package cern.wintercampus.ex6

import cern.wintercampus.utils.createLocalSparkSession
import org.apache.spark.streaming.{Seconds, StreamingContext}

object StreamingExamples {
  def main(args: Array[String]): Unit = {
    val spark = createLocalSparkSession("StreamingApp")
    val ssc = new StreamingContext(spark.sparkContext, Seconds(5))
    val wordCounts: _root_.org.apache.spark.streaming.dstream.DStream[(_root_.java.lang.String, Int)] = wordCountStreaming(ssc)
    wordCounts.print()
    ssc.start() // Start the computation
    ssc.awaitTermination() // Wait for the computation to terminate
  }

  private def wordCountStreaming(ssc: StreamingContext) = {
    val lines = ssc.socketTextStream("localhost", 9999)
    val words = lines.flatMap(_.split(" "))
    val pairs = words.map(word => (word, 1))
    val wordCounts = pairs.reduceByKey(_ + _)
    wordCounts
  }
}

package cern.wintercampus.ex4.dataframe

import org.apache.spark.sql.{DataFrame, SparkSession}

case class DataFrameExamples(spark: SparkSession) {

  import spark.implicits._

  def readCsvAndAggregate(pathToDs: String): Unit = {
    val df = spark.read.option("header", "true").csv(pathToDs)
    // variant 1: DF DSL
    df.printSchema()
    val documentaries = df.where($"_c1" equalTo "Documentary")
      .count()
    println(s"Documentaries: ${documentaries}")

    // variant 2: use raw SQL!
    df.createOrReplaceTempView("genres")
    val results = spark.sql(
      """
        |select count(*) from genres
        |where _c1 = 'Documentary'
        |""".stripMargin)
    results.show()
  }

  def filterWithSchema(pathToDs: String): Unit = {
    val typedDf = readGenresWithSchema(pathToDs)
    val animations = typedDf.where($"genre" === "Animation").count()
    println(s"Animation movies: ${animations}")
  }

  def aggregateWithSchema(pathToDs: String): Unit = {
    val typedDf = readGenresWithSchema(pathToDs)
    val countDf = typedDf.groupBy($"genre")
      .count()
    countDf.printSchema()
    countDf.show()
  }

  def aggregateWithSchemaSql(pathToDs: String): Unit = {
    val typedDf = readGenresWithSchema(pathToDs)
    typedDf.createOrReplaceTempView("genres")
    val results = spark.sql(
      """
        |select count(*) as count from genres
        |where genre = 'Documentary'
        |""".stripMargin)
    results.printSchema()
    results.show()
  }

  private def readGenresWithSchema(pathToDs: String): DataFrame = {
    spark.read.option("header", "true")
      .schema("movie_id INT, genre STRING")
      .csv(pathToDs)
  }

  private def readMoviesWithSchema(pathToDs: String): DataFrame = {
    spark.read.option("header", "true")
      .schema("id INT, name STRING, year INT, rank DOUBLE")
      .csv(pathToDs)
  }

  def joinAndAggregateSql(moviesPath: String, moviesGenresPath: String): Unit = {
    val moviesDf = readMoviesWithSchema(moviesPath)
    val genresDf = readGenresWithSchema(moviesGenresPath)

    moviesDf.createOrReplaceTempView("movies")
    genresDf.createOrReplaceTempView("genres")
    val resultsDf = spark.sql(
      """
        | select m.id, m.name, m.year, m.rank, array_join(collect_set(g.genre), ', ') as concatenated_genres
        | from movies m
        | join genres g on g.movie_id = m.id
        | where m.rank is not null
        | group by m.id, m.name, m.year, m.rank
        |""".stripMargin)
    resultsDf.explain()
    resultsDf.show(10)
  }

  def joinAndAggregateSqlForParquet(moviesPath: String, moviesGenresPath: String): DataFrame = {
    val moviesDf = readMoviesWithSchema(moviesPath)
    val genresDf = readGenresWithSchema(moviesGenresPath)

    moviesDf.createOrReplaceTempView("movies")
    genresDf.createOrReplaceTempView("genres")
    val resultsDf = spark.sql(
      """
        | select m.id, m.name, m.year, m.rank, g.genre
        | from movies m
        | join genres g on g.movie_id = m.id
        | where m.rank is not null
        |""".stripMargin)
    resultsDf.explain()
    resultsDf
  }
}

package cern.wintercampus.ex4.dataframe

import cern.wintercampus.utils.{MovieGenresDatasetPath, MoviesDatasetPath, createLocalSparkSession, sleepAfterRun}

object DataFrameTraining {

  def main(args: Array[String]): Unit = {
    val spark = createLocalSparkSession("DataFrameTraining")

    val examples = DataFrameExamples(spark)
    // uncomment as needed
    //    examples.readCsvAndAggregate(MovieGenresDatasetPath)
    //    examples.filterWithSchema(MovieGenresDatasetPath)
    //    examples.aggregateWithSchema(MovieGenresDatasetPath)
    //    examples.aggregateWithSchemaSql(MovieGenresDatasetPath)
    examples.joinAndAggregateSql(MoviesDatasetPath, MovieGenresDatasetPath)
    sleepAfterRun()
  }

}

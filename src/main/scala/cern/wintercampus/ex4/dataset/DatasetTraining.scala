package cern.wintercampus.ex4.dataset

import cern.wintercampus.utils.{createLocalSparkSession, sleepAfterRun}

object DatasetTraining {
  def main(args: Array[String]): Unit = {
    val spark = createLocalSparkSession("DatasetTraining")
    val examples = DatasetExamples(spark)
//    examples.dataSetExample()
//    examples.joinedDataSetExample()
//    examples.joinedDataSetExampleAndAgg()
//    examples.joinedDatasetUdfs()
    examples.datasetSpecificOperations()
    sleepAfterRun()
  }
}

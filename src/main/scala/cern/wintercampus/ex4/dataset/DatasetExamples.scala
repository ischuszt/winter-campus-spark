package cern.wintercampus.ex4.dataset

import org.apache.spark.sql.SparkSession

case class Person(firstName: String, lastName: String, departmentId: Integer, salary: Integer)
case class Department(id: Integer, name: String)

case class DatasetExamples(spark: SparkSession) {
  import spark.implicits._

  val mockPeople: Seq[Person] = Seq(
    Person("John", "Snow", 1, 3200),
    Person("Andrea", "Bocelli", 1, 3900),
    Person("Grzegorz", "Kowalski", 2, 2800),
    Person("Piotr", "Seweryn", 2, 3000)
  )

  val mockDepartments: Seq[Department] = Seq(
    Department(1, "Accounting"),
    Department(2, "IT")
  )

  def dataSetExample(): Unit = {
    val people = spark.createDataset(mockPeople)
    people.printSchema()
    // show all people with a certain salary
    val filteredPeople = people.where($"salary" >= 3900)
    filteredPeople.explain()
    filteredPeople.show()
  }


  def joinedDataSetExample(): Unit = {
    val people = spark.createDataset(mockPeople)
    val departments = spark.createDataset(mockDepartments)
    // join datasets and show some of the resulting columns
    val filteredPeople = people.join(departments, $"departmentId" === $"id")
      .select($"lastName", $"salary", $"name")
    filteredPeople.explain()
    filteredPeople.show()
  }

  def joinedDataSetExampleAndAgg(): Unit = {
    val people = spark.createDataset(mockPeople)
    val departments = spark.createDataset(mockDepartments)
    // group people by departments and show their avg salaries
    val aggregated = people.join(departments, $"departmentId" === $"id")
      .selectExpr("salary", "name as deptName")
      .groupBy("deptName")
      .agg("salary" -> "avg")
    aggregated.explain()
    aggregated.show()
  }


  def joinedDatasetUdfs(): Unit = {
    val people = spark.createDataset(mockPeople)
    val departments = spark.createDataset(mockDepartments)
    // group people by departments and show their avg salaries
    spark.udf.register("accountingFilter", (dept: String) => { dept == "Accounting"})
    spark.udf.register("increaseSalary", (salary: Int) => { salary + 500 })
    val aggregated = people.join(departments, $"departmentId" === $"id")
      .selectExpr("firstName", "lastName", "salary", "name as deptName")
      .where("accountingFilter(deptName)")
      .selectExpr("firstName", "lastName", "deptName", "increaseSalary(salary)")

    aggregated.explain()
    aggregated.show()
  }

  def datasetSpecificOperations(): Unit = {
    val people = spark.createDataset(mockPeople)
    val groupedByDepartmentId = people.groupByKey(p => p.departmentId)
      .count()

    groupedByDepartmentId.show()
  }


}

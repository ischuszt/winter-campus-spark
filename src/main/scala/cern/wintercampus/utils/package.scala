package cern.wintercampus

import org.apache.spark.sql.SparkSession

import java.io.File
import scala.reflect.io.Directory

package object utils {

  // TODO changeme
  val PageCountsDatasetPath =
    "C:\\Users\\cristi\\Downloads\\winter campus\\spark\\pagecounts\\pagecounts-20140108-090000.gz"
  val MovieGenresDatasetPath = "C:\\Users\\cristi\\Downloads\\winter campus\\spark\\imdb\\imdb_ijs_movies_genres.csv"
  val MoviesDatasetPath = "C:\\Users\\cristi\\Downloads\\winter campus\\spark\\imdb\\imdb_ijs_movies.csv"


  def createLocalSparkSession(appName: String): SparkSession =
    SparkSession.builder.appName(appName)
      .master(s"local[*]")
      .getOrCreate()

  def sleepAfterRun(): Unit = {
    while (true) {
      Thread.sleep(1000)
    }
  }

  def deleteOutputsIfExists(): Unit = {
    val directory = new Directory(new File("outputs"))
    if (directory.exists) {
      directory.deleteRecursively()
    }
  }
}

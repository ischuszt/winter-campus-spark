package cern.wintercampus.ex1

import cern.wintercampus.utils.{createLocalSparkSession, sleepAfterRun}

object WordCountApp {
  def main(args: Array[String]): Unit = {
    val spark = createLocalSparkSession("Word count application")
    val logData = spark.read.textFile("src/main/resources/ex1/README.md").rdd

    logData
      .map(d => d.toLowerCase())
      .map(row => row.split(" "))
      .flatMap(r => r)
      .map(word => (word, 1))
      .reduceByKey((a, b) => a + b)
      .foreach(result => println(result))

    sleepAfterRun()
  }
}

package cern.wintercampus.ex3

import cern.wintercampus.utils._
import org.apache.spark.rdd.RDD

import java.time.{LocalDateTime, ZoneOffset}

object MapReduceWikipedia {

  // TODO changeme
  val filteredCountries: Seq[String] = Seq("pl", "fr", "ch")

  def main(args: Array[String]): Unit = {
    val spark = createLocalSparkSession("MapReduce")

    val rawRdd = spark.sparkContext.textFile(PageCountsDatasetPath)

    rawRdd.map(l => l.split(" "))
      .map(l => (l(0), l(2).toLong))
      .reduceByKey((x, y) => x + y)
      .filter(l => filteredCountries.contains(l._1))
      .foreach(l => println(l))
    //    (ch, 154)
    //    (fr, 928095)
    //    (pl, 568837)

    timeJobs(rawRdd)

    sleepAfterRun()
  }

  private def timeJobs(rawRdd: RDD[String]): Unit = {
    // timing test
    val duration1 = timeJob(() => {
      rawRdd.map(l => l.split(" "))
        .map(l => (l(0), l(2).toLong))
        .reduceByKey((x, y) => x + y)
        .filter(l => filteredCountries.contains(l._1))
        .foreach(l => println(l))
    })

    val duration2 = timeJob(() => {
      rawRdd
        .map(l => l.split(" "))
        .filter(l => filteredCountries.contains(l(0)))
        .map(l => (l(0), l(2).toLong))
        .reduceByKey((x, y) => x + y)
        .foreach(l => println(l))
    })
    println(s"Durations: ${duration1}, ${duration2}")
  }

  private def timeJob(action: () => Unit): String = {
    val start = LocalDateTime.now()
    action()
    val end = LocalDateTime.now()
    val diff = (end.toInstant(ZoneOffset.UTC).toEpochMilli - start.toInstant(ZoneOffset.UTC).toEpochMilli) / 1000
    s"Duration: ${diff}"
  }
}
